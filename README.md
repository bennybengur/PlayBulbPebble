This is an App providing an API to control a PlayBulb Sphere (google it) via intents for Android.
Last year, I got this BT enabled, sphere shaped lamp. I also own a Pebble watch so i thought it could be cool to be able to control the lamp using my pebble, and this how this project was born.
I am not an Android developer and this app is far from being complete, but it works ;)
Since I didn't continue working on it I thought it's best to make the source code public.

I use it with Tasker Actions and Pebble Tasker.

Actions:

Light On -
Type: Send Intent
Action: com.benny.playbulbpebble.action.LIGHT
Extra: SWITCH:ON

Light Off -
Type: Send Intent
Action: com.benny.playbulbpebble.action.LIGHT
Extra: SWITCH:ON

Have Fun!

Benny.