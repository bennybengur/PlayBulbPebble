package com.benny.playbulbpebble;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import net.margaritov.preference.colorpicker.ColorPickerDialog;


public class MainActivity extends AppCompatActivity {
    private Context context = this;
    /*private SharedPreferences sharedPreferences = context.getSharedPreferences(
            getString(R.string.pref_file_key), MODE_PRIVATE);
    private String m_onColor = sharedPreferences.getString(
            getString(R.string.color_picker), getString(R.string.default_on_color)
    );*/
    Button btnColor;
    ColorPickerDialog colorPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        btnColor  = (Button) findViewById(R.id.btnColor);

//        SharedPreferences sharedPreferences = context.getSharedPreferences(getString(R.string.preference_file),
//                MODE_PRIVATE);

        SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        
        //// TODO: 10/3/2017 change bt_device_name to bt_device_address 
        if (prefs.getString("bt_device_name", "") == "0"){
            CharSequence text = "No Device selected!";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }

        btnColor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                PlayBulbIntentService.startActionLight(context.getApplicationContext(), "ON");
            }
        });

       /* btnColor.setBackgroundColor(Color.parseColor("#" + sharedPreferences.getString(getString(R.string.on_color_pref),
                getString(R.string.default_on_color))));

        btnColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorPicker = new ColorPickerDialog(MainActivity.this, Color.WHITE);
                colorPicker.setHexValueEnabled(true);
                colorPicker.setAlphaSliderVisible(true);
                colorPicker.setTitle("Pick a color:");
                colorPicker.setOnColorChangedListener(new ColorPickerDialog.OnColorChangedListener() {
                    @Override
                    public void onColorChanged(int color) {
                        btnColor.setBackgroundColor(color);
                        SharedPreferences sharedPreferences = context.getSharedPreferences(getString(R.string.preference_file),
                                MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(getString(R.string.on_color_pref), Integer.toHexString(color));
                        editor.commit();
                        // TODO: save settings
                        Log.d("color", sharedPreferences.getString(getString(R.string.on_color_pref),
                                getString(R.string.default_on_color)));
                    }
                });
                colorPicker.show();
            }
        });*/

    }

    @Override
    protected void onResume(){
        super.onResume();
        /*Button btnColor = (Button)findViewById(R.id.btnColor);
        btnColor.setBackgroundColor(Color.parseColor("#" + m_onColor));*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mnuSettings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
