package com.benny.playbulbpebble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.util.Log;

import java.util.StringTokenizer;
import java.util.UUID;

public class PlayBulbBL {
    private Context m_context;
    private String m_light;
    private String m_device;

    private static final String TAG = "PlayBulbBL";
    private static final UUID GATT_SERVICE = UUID.fromString("0000FF08-0000-1000-8000-00805F9B34FB");
    private static final UUID GATT_CHARAC = UUID.fromString("0000FFFC-0000-1000-8000-00805F9B34FB");

    public PlayBulbBL(Context context, String light, String device){
        m_context = context;
        m_light = light;
        m_device = device;
    }

    public void connect(){
        final BluetoothManager bluetoothManager =
                (BluetoothManager) m_context.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();
        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(m_device);
        device.connectGatt(m_context, true, mGattCallback, 2); // false?
    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                            int newState) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothGatt.STATE_CONNECTED) {
                    gatt.discoverServices();
                } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                    gatt.close();
                }
            } else {
                gatt.disconnect();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS){
                BluetoothGattService service = gatt.getService(GATT_SERVICE);
                if (service != null){
                    BluetoothGattCharacteristic charac = service.getCharacteristic(GATT_CHARAC);
                    if (charac!=null ){
                        setLight(gatt, charac);
                    }else {
                        Log.d(TAG, "Error: character does not exist.");
                    }
                } else {
                    Log.d(TAG, "Error: service is not in list of discovered services.");
                    gatt.disconnect();
                }
            } else{
                Log.d(TAG, "Error: could not discover services. status code: " + status);
                gatt.disconnect();
            }
        }

        @Override
        public void  onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic charac, int status){
            if (status == BluetoothGatt.GATT_SUCCESS){

                Log.d(TAG, "CharacteristicWrite: success" );
            }
            else{
                Log.d(TAG, "CharacteristicWrite failed. status: " + status);
            }
            //closeConnection();
            gatt.disconnect();
        }
    };

    /*public enum Light{
        LIGHT_ON("FF000000"), // White
        LIGHT_OFF("00000000"); // Black

        private final String light;
        Light(String light) { this.light = light; }
        public String getValue() { return light; }
    }*/


    private void setLight(BluetoothGatt gatt, BluetoothGattCharacteristic charac){
        charac.setValue(hexStringToByteArray(m_light));
        Log.d(TAG, "string value: " + m_light);
        gatt.writeCharacteristic(charac);
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        //Log.d(TAG, "data value: " + data.);
        return data;
    }
//
//    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
//    private static String bytesToHex(byte[] bytes) {
//        char[] hexChars = new char[bytes.length * 2];
//        for ( int j = 0; j < bytes.length; j++ ) {
//            int v = bytes[j] & 0xFF;
//            hexChars[j * 2] = hexArray[v >>> 4];
//            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
//        }
//        return new String(hexChars);
//    }
}
