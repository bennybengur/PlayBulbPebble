package com.benny.playbulbpebble;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PlayBulbIntentService extends IntentService {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String SET_LIGHT = "com.benny.playbulbpebble.action.LIGHT";
    private static final String EXTRA_SWITCH = "com.benny.playbulbpebble.extra.SWITCH";
    private static final String ON_SWITCH = "ON";
    private static final String OFF_SWITCH = "OFF";
    private PlayBulbBL playBulbBL;

    public PlayBulbIntentService() {
        super("PlayBulbIntentService");
    }

    // Helper Method
    public static void startActionLight(Context context, String param) {
        Intent intent = new Intent(context, PlayBulbIntentService.class);
        intent.setAction(SET_LIGHT);
        intent.putExtra(EXTRA_SWITCH, param);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (SET_LIGHT.equals(action)) {
                final String param = intent.getStringExtra(EXTRA_SWITCH);
                handleActionSetLight(param);
            }
        }
    }

    private void handleActionSetLight(String param) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String device = prefs.getString("bt_device_name","0");
        if (param.equals(ON_SWITCH)){
            int value = prefs.getInt("onColor", 0);
            String color = Integer.toHexString(value);
            playBulbBL = new PlayBulbBL(this, color, device);
        } else if (param.equals(OFF_SWITCH)){
            playBulbBL = new PlayBulbBL(this, "00000000", device);
        }
        playBulbBL.connect();
    }

}
