package com.benny.playbulbpebble;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class PlayBulbReceiver extends BroadcastReceiver {
    public PlayBulbReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data = intent.getExtras();
        String switchLight = data.getString("SWITCH");
        PlayBulbIntentService.startActionLight(context.getApplicationContext(), switchLight);
    }
}
