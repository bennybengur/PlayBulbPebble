package com.benny.playbulbpebble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by BenGur on 04/08/2017.
 */

public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        ListPreference listPreferenceBtDevice = (ListPreference) findPreference("bt_device_name");
        setDataForBTList(listPreferenceBtDevice);


    }

    Preference.OnPreferenceClickListener btListPreferenceClickListener = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            ListPreference listPreference = (ListPreference)preference;
            if (listPreference.getEntryValues()[0].equals("0")){
                if (!setDataForBTList(listPreference)){
                    listPreference.getDialog().dismiss();
                    Log.d("List", "onPreferenceClick: dialog dismissed");
                }
            }

            return true;
        }
    };

    private boolean setDataForBTList(ListPreference listPreferenceBtDevice){
        boolean isPopulatedWithDevices = false;
        if (listPreferenceBtDevice  != null) {

            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null){
                Context context = getContext();
                CharSequence text = "No Bluetooth adapter found!";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                listPreferenceBtDevice.setOnPreferenceClickListener(btListPreferenceClickListener);

                return isPopulatedWithDevices;
            }

            CharSequence entries[];
            CharSequence entryValues[];
            if (mBluetoothAdapter.isEnabled()){
                Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

                if(pairedDevices.size() > 0){
                    entries = new String[pairedDevices.size()];
                    entryValues = new String[pairedDevices.size()];
                    int i = 0;
                    for (BluetoothDevice device : pairedDevices) {
                        entries[i] = device.getName();
                        entryValues[i] = device.getAddress();
                        i++;
                        isPopulatedWithDevices = true;
                    }
                }else {
                    entries = new String[0];
                    entryValues = new String[0];
                    entries[0] = "No Devices paired.";
                    entryValues[0] = "0";
                    isPopulatedWithDevices = false;
                }
            }else {
                entries = new String[0];
                entryValues = new String[0];
                entries[0] = "Bluetooth is not Enabled.";
                entryValues[0] = "0";
                isPopulatedWithDevices = false;
            }

            listPreferenceBtDevice.setEntries(entries);
            listPreferenceBtDevice.setEntryValues(entryValues);
        }
        return isPopulatedWithDevices;
    }
}
